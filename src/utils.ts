import * as chalk from 'chalk';
import { spawn } from 'child_process';

export function spawnPromise(cmd: string, args: string[], onStd?: (std) => void) {
  return new Promise<string>((resolve, reject) => {
    console.log(chalk.default.dim(`\t\t\t\t\t\t\t$ ${cmd} ${args.join(' ')}`));
    const diffProcess = spawn(cmd, args, {shell: true});
    let stdo = '';
    let err = '';
    diffProcess.stdout.on('data', d => {
      const str = d.toString();
      if (onStd) {
        onStd(str);
      }
      stdo += str;
    });

    diffProcess.stderr.on('data', d => err += d.toString());

    diffProcess.on('exit', code => {
      if (code === 0) {
        return resolve(stdo);
      }
      reject(err);
    });
  });
}
