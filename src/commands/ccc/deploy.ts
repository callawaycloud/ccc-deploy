import { flags, SfdxCommand } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as chalk from 'chalk';
import { join } from 'path';
import { spawnPromise } from '../../utils';
// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('ccc-deploy', 'org');

export default class Org extends SfdxCommand {

  public static description = messages.getMessage('commandDescription');

  public static examples = [
    '$ sfdx ccc:deploy -d my-production-user',
    '$ sfdx ccc:deploy -d my-production-user -s 4'
  ];

  public static args = [{ name: 'file' }];

  protected static flagsConfig = {
    // flag with a value (-n, --name=VALUE)
    deploytarget: flags.string({ char: 'd', description: messages.getMessage('deploytargetDescription'), required: true }),
    step: flags.number({ char: 's', description: messages.getMessage('stepDescription'), default: 0 })
  };

  // Comment this out if your command does not require an org username
  protected static requiresUsername = true;

  // Comment this out if your command does not support a hub org username
  protected static supportsDevhubUsername = false;

  // Set this to true if your command requires a project workspace; 'requiresProject' is false by default
  protected static requiresProject = false;

  private currentBranch: string;
  private productionBranch: string;
  private devUser: string;
  private targetUser: string;
  private packagePath: string;

  private precheck = async (): Promise<boolean> => {
    const status = await spawnPromise('git', ['status', '--porcelain']);
    if (status.length) {
      this.quit(`You have uncommitted work: \n${chalk.default.reset(status)} \nPlease commit/reset/ignore until there are no pending changes and try again`);
      return false;
    }

    if (this.currentBranch.toLowerCase() === this.productionBranch) {
      this.quit('You cannot run this command FROM master.  Please checkout the feature branch you wish to deploy and retry!');
      return false;
    }
    return true;
  }

  private confirmCheckedIn = async (): Promise<boolean> => {
    const confirmedCheckedIn = await this.prompt(`Are you sure ${chalk.default.bold('EVERY')} change has been checked into ${chalk.default.bold(this.currentBranch)}? (${chalk.default.greenBright('y')})`);
    if (confirmedCheckedIn.toLowerCase() !== 'y') {
      const retrieveSource = await this.prompt(`Would you like to refresh source from ${this.devUser} now? (${chalk.default.greenBright('y')})`);
      if (retrieveSource.toLowerCase() !== 'y') {
        await spawnPromise('sfdx', ['force:source:retrieve', '--manifest', 'manifest/package.xml', '-u', this.devUser], s => this.ux.log(s));
        this.info('Project Refreshed');
      }
      this.quit('Review changes to ensure that you have checked in all work then run this command again.');
      return false;
    }
    return true;
  }

  private confirmPR = async (): Promise<boolean> => {
    const prSubmitted = await this.prompt(`Have you ${chalk.default.bold('submitted a Pull Request?')} (${chalk.default.greenBright('y')})`);
    if (prSubmitted.toLowerCase() !== 'y') {
      this.quit('Please submit a PR and see me again');
      return false;
    }
    return true;
  }

  private syncProductionWithMaster = async (): Promise<boolean> => {
    this.heading(`Syncing production -> ${this.productionBranch}`);
    const syncProd = await this.prompt(`Continue? (${chalk.default.greenBright('y')} | skip)`);
    if (syncProd.toLowerCase() === 'skip') {
      this.ux.warn('Hope you know what you\'re doing...');
    } else if (syncProd.toLowerCase() !== 'y') {
      this.quit('GoodBye!');
      return false;
    } else {
      await spawnPromise('git', ['checkout', this.productionBranch]);
      await spawnPromise('git', ['reset', '--hard']); // I think this is needed in case there are changes on the working tree
      await spawnPromise('git', ['pull']);
      this.ux.startSpinner(chalk.default.dim(`refreshing source from ${this.targetUser}`));
      await spawnPromise('sfdx', ['force:source:retrieve', '--manifest', 'manifest/package.xml', '-u', this.targetUser]);
      this.ux.stopSpinner(chalk.default.green('source refreshed'));

      const productionChanges = await spawnPromise('git', ['status', '--porcelain']);
      if (productionChanges.length) {
        this.info(`Found the following changes in production \n ${productionChanges}`);
        const commitSync = await this.prompt(`Commit & Push? (${chalk.default.greenBright('y')})`);
        if (commitSync.toLowerCase() !== 'y') {
          this.quit(`Please manually sync ${this.productionBranch} and production and then try again.`);
          return;
        }
        await spawnPromise('git', ['add', '.']);
        await spawnPromise('git', ['commit', '-m', "'[automated] Adding un-tracked changes from production'"]);
        await spawnPromise('git', ['push']);
      } else {
        this.info('No changes found in production!');
      }
    }
    return true;
  }

  private mergeMasterIntoBranch = async (): Promise<boolean> => {
    // merge master into staging
    this.heading(`Attempting to ${chalk.default.bold('merge')} ${this.productionBranch} into ${chalk.default.bold(this.currentBranch)}`);
    const merge = await this.prompt(`continue? (${chalk.default.greenBright('y')})`);
    if (merge.toLowerCase() !== 'y') {
      this.quit('I could tell you didn`t have it in ya');
      return false;
    }
    await spawnPromise('git', ['checkout', this.currentBranch]);
    await spawnPromise('git', ['merge', '--no-edit', this.productionBranch]); // capture conflicts?
    return true;
  }

  private buildDeploymentPackage = async (): Promise<boolean> => {
    this.heading('Building Deployment Package');

    await spawnPromise('sfdx', ['git:package', '-s', this.currentBranch, '-d', this.packagePath]);
    const packageXml = await spawnPromise('cat', [join(this.packagePath, 'package.xml')]);
    this.heading('Package Created');
    this.info('\n' + packageXml);
    return true;
  }

  private sendIt = async (): Promise<boolean> => {
    const sendIt = await this.prompt(`\n\n${chalk.default.underline.bold.redBright('Send it?')} (${chalk.default.greenBright('y')})`);

    if (sendIt.toLowerCase() !== 'y') {
      this.quit('If you scared go to church');
      return false;
    }
    await spawnPromise('sfdx', ['force:mdapi:deploy', '-d', this.packagePath, '-w', '5000', '-u', this.targetUser], s => this.ux.log(s));
    this.heading(chalk.default.green('Deployment Succeeded!'));
    return true;
  }

  private mergeIntoMaster = async (): Promise<boolean> => {
    this.heading(`Merging in to ${this.productionBranch}`);
    await spawnPromise('git', ['checkout', 'master']);
    await spawnPromise('git', ['merge', '--no-edit', this.currentBranch]);
    return true;
  }

  private compareDeploymentPackage = async (): Promise<boolean> => {
    this.heading('Checking for packaging differences');
    this.ux.startSpinner(chalk.default.dim(`refreshing source from ${this.targetUser}`));
    await spawnPromise('sfdx', ['force:source:retrieve', '--manifest', 'manifest/package.xml', '-u', this.targetUser]);
    this.ux.stopSpinner(chalk.default.green('source refreshed'));
    try {
      await spawnPromise('sfdx', ['git:package', '-d', this.packagePath, '-w']); // this will err if there are no changes
      this.quit('Found packagable changes after deployment...  Please review and finish merge manually');
      return false;
    } catch (e) {
      // this.info(chalk.default.green('No Change'));
    }
    this.info(chalk.default.green('Pushing Master'));
    await spawnPromise('git', ['push']);
    return true;
  }

  // tslint:disable-next-line: member-ordering
  public async run(): Promise<AnyJson> {
    this.heading('Welcome to CCC deploy integration continua machina');
    this.ux.log(chalk.default.magentaBright(`
      \\_/
     (* *)
    __)#(__
   ( )...( )(_)   C C C
   || |_| ||//
>==() | | ()/
    _(___)_
   [-]   [-]
`));

    // setup instance members
    this.currentBranch = (await spawnPromise('git', ['rev-parse', '--abbrev-ref', 'HEAD'])).trim();
    this.productionBranch = 'master'; // [todo]: make configurable?
    this.devUser = this.org.getUsername();
    this.targetUser = this.flags.deploytarget;
    this.packagePath = join('deploy', this.currentBranch);

    this.heading(`Deploying Branch ${chalk.default.redBright(this.currentBranch)} to ORG ${chalk.default.redBright(this.targetUser)}`);

    // do precheck
    if (!(await this.precheck())) {
      return;
    }

    // run through each step
    const steps: Array<() => Promise<boolean>> = [
      this.confirmCheckedIn,
      this.confirmPR,
      this.syncProductionWithMaster,
      this.mergeMasterIntoBranch,
      this.buildDeploymentPackage,
      this.sendIt,
      this.mergeIntoMaster,
      this.compareDeploymentPackage
    ];

    for (let i = this.flags.step; i < steps.length; i++) {
      const step = steps[i];
      const success = await step();
      if (!success) {
        this.info('To resume at current step, run command again with: -s ' + i);
        return;
      }
    }

    this.info(String.raw`
    .''.      .        *''*    :_\/_:     .
    :_\/_:   _\(/_  .:.*_\/_*   : /\ :  .'.:.'.
.''.: /\ :   ./)\   ':'* /\ * :  '..'.  -=:o:=-
:_\/_:'.:::.    ' *''*    * '.\'/.' _\(/_'.':'.'
: /\ : :::::     *_\/_*     -= o =-  /)\    '  *
'..'  ':::'     * /\ *     .'/.\'.   '
    *            *..*         :
jgs     *
      *
    `);
    }

  private prompt(s: string): Promise < string > {
    return this.ux.prompt('  ' + chalk.default.magenta(s));
  }

  private info(s: string) {
    return this.ux.log('    ' + chalk.default.cyan(s));
  }

  private heading(s: string) {
    this.ux.log(chalk.default.greenBright.bold('\n=== ' + s + ' ==='));
  }

  private quit(s: string) {
    this.ux.log(chalk.default.redBright(s));
  }
}
