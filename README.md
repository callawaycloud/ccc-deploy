# ccc-deploy

## Setup

1. clone
1. cd
1. npm/yarn install
1. `sfdx plugins link`

## Usage

1. Open a sfdx project
1. checkout the branch you wish to deploy
1. run `sfdx ccc:deploy -d your-user`

